export interface IHeroProps {
  image?: string | undefined;
  element?: string | undefined;
  children: JSX.Element;
}

export default IHeroProps;
