import { Catalog } from '@/modules/Catalogs/entities/catalog';

export type Partner = {
  id: string;
  logo?: string;
  name: string;
  slogan?: string;
  website?: string;
  catalogs?: Catalog[];
};
