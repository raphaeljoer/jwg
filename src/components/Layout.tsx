import { Box } from '@chakra-ui/react';
import React from 'react';
import Whatsapp from './atoms/buttons/whatsapp';
import NavBar from './molecules/NavBar';

type LayoutProps = {
  children: React.ReactNode | React.ReactNode[];
};

export default function Layout({ children }: LayoutProps) {
  return (
    <Box minW="sm" pos="relative">
      <Whatsapp />
      <NavBar />
      {children}
    </Box>
  );
}
