import React from 'react';
import { AspectRatio, AspectRatioProps } from '@chakra-ui/react';

type VideoType = AspectRatioProps & {
  video_id: string;
};

export const Video = ({ video_id, ...props }: VideoType) => {
  return (
    <AspectRatio ratio={16 / 9} overflow="hidden" {...props}>
      <iframe
        src={`https://www.youtube.com/embed/${video_id}`}
        allowFullScreen
      />
    </AspectRatio>
  );
};
