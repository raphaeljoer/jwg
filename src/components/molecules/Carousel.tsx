//chakra-ui
import {
  Flex,
  FlexProps,
  HStack,
  IconButton,
  ResponsiveValue,
  useBreakpointValue,
} from '@chakra-ui/react';
//core components
import { Container } from './Container';
import Tooltip from '../atoms/Tooltip';

//resources
import React, { useRef } from 'react';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import Heading from '../atoms/Heading';

interface ButtonProps {
  labelRight: string;
  labelLeft: string;
}

type Config = {
  scrollStep: number;
  spacing: number | ResponsiveValue<number>;
  padding: number | ResponsiveValue<number>;
  title?: string;
  buttonProps?: ButtonProps;
  navigationProps?: FlexProps;
};

interface CarouselProps extends FlexProps {
  config: Config;
  children: any | any[];
}

export const Carousel = ({ config, children, ...props }: CarouselProps) => {
  const partnersRef = useRef<HTMLDivElement>(null);
  const isTablet = useBreakpointValue({ base: false, md: true });

  const handleScrollNext = () => {
    partnersRef.current?.scrollBy(config.scrollStep, 0);
  };

  const handleScrollBack = () => {
    partnersRef.current?.scrollBy(-config.scrollStep, 0);
  };

  const displayNavigation = () => (
    <Container>
      <Flex
        w="full"
        justifyContent={config.title ? 'space-between' : 'flex-end'}
        mb={8}
        {...config.navigationProps}
      >
        {config.title && (
          <Heading
            fontSize={32}
            color="oilblue.500"
            m={{ base: 'auto', md: 0 }}
          >
            {config.title}
          </Heading>
        )}
        {isTablet && (
          <HStack>
            <Tooltip label="Voltar" placement="top" {...props}>
              <IconButton
                colorScheme="oilblue"
                aria-label="Scroll Back"
                mr={2}
                icon={<IoIosArrowBack />}
                onClick={handleScrollBack}
              />
            </Tooltip>
            <Tooltip label="Próximo" placement="top" {...props}>
              <IconButton
                colorScheme="oilblue"
                aria-label="Scroll Forward"
                icon={<IoIosArrowForward />}
                onClick={handleScrollNext}
              />
            </Tooltip>
          </HStack>
        )}
      </Flex>
    </Container>
  );

  return (
    <>
      {displayNavigation()}
      <Flex
        ref={partnersRef}
        overflowX="scroll"
        css={{
          scrollSnapType: 'x mandatory',
          WebkitScrollSnapType: 'x mandatory',
          scrollBehavior: 'smooth',
          WebkitScrollBehavior: 'smooth',
          WebkitOverflowScrolling: 'touch',
          msOverflowStyle: 'none' /* IE and Edge */,
          scrollbarWidth: 'none' /* Firefox */,
          '&::-webkit-scrollbar': { display: 'none' },
        }}
        {...props}
      >
        <HStack spacing={config.spacing} px={config.padding}>
          {children}
        </HStack>
      </Flex>
    </>
  );
};

export default Carousel;
