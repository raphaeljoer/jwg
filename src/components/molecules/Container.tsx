//chakra-ui
import {
  Box,
  BoxProps,
  Container as CkContainer,
  ContainerProps,
} from '@chakra-ui/react';

//resources
import React from 'react';

type ContainerType = BoxProps & {
  children: any;
  containerProps?: ContainerProps;
};

export const Container = ({
  children,
  containerProps,
  ...props
}: ContainerType) => {
  return (
    <Box {...props}>
      <CkContainer maxW="container.xl" {...containerProps}>
        {children}
      </CkContainer>
    </Box>
  );
};

export default Container;
