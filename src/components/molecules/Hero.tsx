//chakra-ui
import {
  useBreakpointValue,
  Flex,
  Box,
  ContainerProps,
} from '@chakra-ui/react';

//core components
import Scroll from '@/components/atoms/Scroll';
import Container from '@/components/molecules/Container';

//resources
import React, { cloneElement } from 'react';
import Image from 'next/image';
import { ui } from '@/config/app';

//types
import ICtaProps from '@/@types/cta';
import IHeroProps from '@/@types/hero';

const childrenProps: ICtaProps = {
  w: 'full',
  maxW: 'lg',
  m: { base: 'auto', lg: 0 },
  justifyContent: 'center',
  titleProps: {
    color: 'oilblue.10',
    textAlign: { base: 'center', lg: 'left' },
  },
  descriptionProps: {
    color: 'oilblue.50',
    maxW: 'xs',
    m: { base: 'auto', lg: 0 },
    textAlign: { base: 'center', lg: 'left' },
  },
};

const containerProps: ContainerProps = {
  pt: 16,
  h: { base: 540, sm: 600, md: 680 },
  alignContent: 'space-between',
  pos: 'relative',
};

const displayHeroPicture = (
  element: IHeroProps['element'],
  image: IHeroProps['image'],
) => (
  <Box position="relative" w="80%" h="100%" ml="auto" alignItems="center">
    {element && (
      <Image
        src={element}
        alt="JWG tool"
        layout="fill"
        objectFit="contain"
        objectPosition="100% 70%"
      />
    )}

    {image && (
      <Image
        src={image}
        alt="JWG"
        layout="fill"
        objectFit="contain"
        objectPosition="100% 100%"
      />
    )}
  </Box>
);

const Hero = ({
  element,
  image,
  children,
  ...props
}: IHeroProps): JSX.Element => {
  if (!children) throw new Error('Children is mandatory');
  const isDesktop = useBreakpointValue({ base: false, lg: true });
  return (
    <Container as="section" w="full" bgColor="oilblue.800" {...props}>
      <Flex {...containerProps}>
        {cloneElement(children, { ...childrenProps, ...children.props })}
        {isDesktop && displayHeroPicture(element, image)}
        <Scroll file={ui.scroll.src.dark.down} />
      </Flex>
    </Container>
  );
};

export default Hero;
