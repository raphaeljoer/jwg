import mainMenu from '@/data/static/menu';
import {
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Icon,
  IconButton,
  Stack,
  useDisclosure,
} from '@chakra-ui/react';
import React from 'react';
import { HiMenuAlt3 } from 'react-icons/hi';
import ButtonCta from '../atoms/buttons/ButtonCta';
import Link from '../atoms/Link';
import Tooltip from '../atoms/Tooltip';
import Footer from './Footer';
import Logo from './Logo';

const displayDrawer = (isDrawerOpen: boolean, onDrawerClose: () => void) => (
  <Drawer
    placement="right"
    onClose={onDrawerClose}
    isOpen={isDrawerOpen}
    size="full"
  >
    <DrawerOverlay>
      <DrawerContent bgColor="oilblue.800">
        <DrawerHeader>
          <DrawerCloseButton color="blue.500" m={4} size="full" p={4} />
          <Logo my={4} />
        </DrawerHeader>
        <DrawerBody p={0} alignItems="space-between">
          <Stack
            spacing={6}
            mx={4}
            mb={24}
            flexDirection={'column'}
            alignItems={'flex-start'}
          >
            {mainMenu.map(({ label, link, icon }, index) => {
              return (
                <Link
                  key={`${label}-${index}`}
                  href={link}
                  wait={300}
                  onClick={onDrawerClose}
                  ckLinkProps={{ w: 'full' }}
                >
                  <ButtonCta
                    variant="mobileMenu"
                    color="transparent"
                    leftIcon={<Icon as={icon} color="orange.500" mr={4} />}
                  >
                    {label}
                  </ButtonCta>
                </Link>
              );
            })}
          </Stack>
          <Footer hideLogo />
        </DrawerBody>
      </DrawerContent>
    </DrawerOverlay>
  </Drawer>
);

export default function MenuMobile() {
  const {
    isOpen: isDrawerOpen,
    onToggle: onDrawerToggle,
    onClose: onDrawerClose,
  } = useDisclosure();
  return (
    <>
      <Tooltip label="Menu" placement="left-start">
        <IconButton
          onClick={onDrawerToggle}
          bg="transparent"
          colorScheme="orange"
          color="oilblue.10"
          aria-label="Menu"
          fontSize="32px"
          size="lg"
          icon={<HiMenuAlt3 />}
          cursor="pointer"
        />
      </Tooltip>
      {displayDrawer(isDrawerOpen, onDrawerClose)}
    </>
  );
}
