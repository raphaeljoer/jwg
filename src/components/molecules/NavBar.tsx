//chakra-ui
import {
  ButtonGroup,
  Flex,
  Grid,
  Icon,
  Stack,
  useBreakpointValue,
} from '@chakra-ui/react';
//core components
import Link from '@/components/atoms/Link';
import Container from '@/components/molecules/Container';
import Logo from '@/components/molecules/Logo';
import ButtonCta from '@/components/atoms/buttons/ButtonCta';
//resources
import React from 'react';
import { FaWhatsapp } from 'react-icons/fa';
import { whatsapp, zIndex } from '@/config/app';
//data
import social from '@/data/static/social';
import mainMenu from '@/data/static/menu';
import MenuMobile from './MenuMobile';

const displayMenu = () => (
  <Flex gridArea="menu" px={6}>
    <ButtonGroup spacing={2}>
      {mainMenu.map(({ label, link, icon }, index) => (
        <Link key={`${label}-${index}`} href={link} passHref>
          <ButtonCta
            variant="mainMenu"
            color="transparent"
            leftIcon={<Icon as={icon} color="orange.500" />}
          >
            {label}
          </ButtonCta>
        </Link>
      ))}
    </ButtonGroup>
  </Flex>
);

const displaySocial = () => (
  <Flex gridArea="social">
    <Stack direction="row" spacing={6} m="0 auto">
      {social.map(({ label, link, icon }, index) => (
        <Link key={`${label}-${index}`} href={link}>
          <Icon
            as={icon}
            color="oilblue.50"
            fontSize={20}
            _hover={{ color: 'orange.500' }}
          />
        </Link>
      ))}
    </Stack>
  </Flex>
);

const displayContactButton = () => {
  return (
    <Flex gridArea="rightMenu" align="flex-end">
      <ButtonCta
        variant="rightMenu"
        color="orange2"
        leftIcon={<Icon as={FaWhatsapp} fontSize={24} />}
        onClick={() => (location.href = whatsapp.link)}
      >
        Contato
      </ButtonCta>
    </Flex>
  );
};

const displayLogo = () => (
  <Flex gridArea="logo" alignItems="center" h="full">
    <Logo />
  </Flex>
);

const displayRightMenu = (isDesktop: boolean | undefined) => (
  <Flex gridArea="rightMenu" align="flex-end">
    {isDesktop ? displayContactButton() : <MenuMobile />}
  </Flex>
);

export const NavBar = () => {
  const isDesktop = useBreakpointValue({ base: false, lg: true });
  return (
    <Container
      as="nav"
      w="full"
      minW="sm"
      zIndex={zIndex.low}
      position="absolute"
    >
      <Grid
        h={24}
        pt={2}
        templateRows="1fr"
        templateColumns={{ base: '1fr auto', lg: 'auto 1fr 1fr auto' }}
        templateAreas={{
          base: "'logo rightMenu'",
          lg: "'logo menu social rightMenu'",
        }}
        alignItems="center"
      >
        {displayLogo()}
        {isDesktop && displayMenu()}
        {isDesktop && displaySocial()}
        {displayRightMenu(isDesktop)}
      </Grid>
    </Container>
  );
};

export default NavBar;
