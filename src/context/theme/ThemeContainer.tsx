import { theme } from '@/styles/theme';
import { ChakraProvider } from '@chakra-ui/react';

interface ThemeContainerProps {
  children: any;
}

export const ThemeContainer = ({ children }: ThemeContainerProps) => {
  return <ChakraProvider theme={theme}>{children}</ChakraProvider>;
};

export default ThemeContainer;
