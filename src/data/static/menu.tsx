import { FiHome, FiAward, FiTool, FiFlag } from 'react-icons/fi';

export const mainMenu = [
  {
    label: 'Início',
    link: '/',
    icon: FiHome,
  },
  {
    label: 'Catálogos',
    link: '/catalogos',
    icon: FiTool,
  },
  {
    label: 'Campanhas',
    link: '/campanhas',
    icon: FiFlag,
  },
  {
    label: 'Sobre',
    link: '/#sobre',
    icon: FiAward,
  },
];

export default mainMenu;
