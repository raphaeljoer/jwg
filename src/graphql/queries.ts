import { gql } from 'graphql-request';

export const GET_CATALOGS = gql`
  query getCatalogs($first: Int) {
    catalogs(first: $first) {
      id
      name
      releaseDate
      url
      partner {
        name
        logo {
          url
        }
      }
    }
  }
`;

export const GET_CLIENTS = gql`
  query getClients($first: Int) {
    clients(first: $first) {
      id
      name
      logo {
        url
      }
      coverage
    }
  }
`;

export const GET_CAMPAIGNS = gql`
  query getCampaigns($first: Int) {
    campaigns(first: $first) {
      id
      name
      active_status
      activityInterval
      url
      image {
        url
      }
    }
  }
`;
