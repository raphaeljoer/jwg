//chakra-ui
import { Badge, Box, Image as CkImage, Flex } from '@chakra-ui/react';

//core components
import Text from '@/components/atoms/Text';

//resources
import React from 'react';
//types
import { CampaignModel } from '../entities/CampaignModel';
import Link from '@/components/atoms/Link';
import ButtonCta from '@/components/atoms/buttons/ButtonCta';
import { FiDownload } from 'react-icons/fi';
import { zIndex } from '@/config/app';

type PropsType = {
  data: CampaignModel;
};

export const CampaignsCard = ({ data }: PropsType) => {
  return (
    <Flex
      w="full"
      flexDir="column"
      minW={320}
      minH={345}
      maxW={{ base: 'xs', lg: 'sm' }}
      textDecoration="none"
      borderRadius="xl"
      overflow="hidden"
      css={{
        scrollSnapAlign: 'center',
        WebkitScrollSnapAlign: 'center',
      }}
    >
      {data.image && (
        <Flex borderTopRadius="lg">
          <Box position="relative" height="100%">
            <CkImage
              src={data.image.url}
              alt="Catálogo"
              layout="fill"
              objectFit="cover"
            />
          </Box>
        </Flex>
      )}

      <Flex
        flexDir="column"
        h="full"
        bgColor="oilblue.500"
        borderBottomRadius="xl"
        p={6}
      >
        <Badge
          mt={-10}
          mb={6}
          p={2}
          w="fit-content"
          borderRadius="md"
          bgColor={data.active_status ? 'green.500' : 'red.500'}
          color="white"
          textTransform="uppercase"
          zIndex={zIndex.high}
        >
          {data.active_status ? 'Ativa' : 'Encerrada'}
        </Badge>

        <Text
          fontSize={24}
          fontWeight="bold"
          color="whiteAlpha.900"
          mb={2}
          mt="auto"
        >
          {data.activityInterval}
        </Text>

        <Link href={data.url} target="_blank">
          <ButtonCta
            variant="hero"
            color="orange1"
            leftIcon={<FiDownload />}
            w="full"
            mt={4}
          >
            {'Baixar Resultados'}
          </ButtonCta>
        </Link>
      </Flex>
    </Flex>
  );
};
