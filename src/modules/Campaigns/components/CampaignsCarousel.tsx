//resources
import React from 'react';
//core-component
import { CampaignModel } from '../entities/CampaignModel';
import { CampaignsCard } from './CampaignsCard';
import Carousel from '@/components/molecules/Carousel';

type PropsType = {
  data: CampaignModel[];
};

export const CampaignsCarousel = ({ data }: PropsType) => {
  return (
    <Carousel
      config={{
        scrollStep: 384,
        padding: { base: 4, md: 8, lg: 12 },
        spacing: { base: 4, md: 8 },
      }}
    >
      {data.map((d) => (
        <CampaignsCard key={d.id} data={d} />
      ))}
    </Carousel>
  );
};
