//resources
import React from 'react';
//core-component
import { CampaignModel } from '../entities/CampaignModel';
import { CampaignsCard } from './CampaignsCard';
//utils
import Container from '@/components/molecules/Container';
import { Grid } from '@chakra-ui/react';

type PropsType = {
  data: CampaignModel[];
};

export const CampaignsGrid = ({ data }: PropsType) => {
  return (
    <Container>
      <Grid
        templateColumns={{
          base: 'repeat(1, 1fr)',
          md: 'repeat(2, 1fr)',
          xl: 'repeat(3, 1fr)',
        }}
        gap={8}
        rowGap={12}
        mt={24}
        justifyItems="center"
      >
        {data.map((d) => (
          <CampaignsCard key={d.id} data={d} />
        ))}
      </Grid>
    </Container>
  );
};
