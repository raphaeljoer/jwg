//chakra-ui
import { Box, BoxProps, Image, Heading } from '@chakra-ui/react';
//resources
import React from 'react';
//type
import { CampaignModel } from '../entities/CampaignModel';
import { CampaignsCarousel } from './CampaignsCarousel';
import { CampaignsGrid } from './CampaignsGrid';
import Scroll from '@/components/atoms/Scroll';
import { ui } from '@/config/app';
import Container from '@/components/molecules/Container';

type VariantType = 'carousel' | 'grid';

type PropsType = BoxProps & {
  data: CampaignModel[];
  variant: VariantType;
  withScroll?: boolean;
};

export const CampaignsSection = ({
  data,
  variant,
  withScroll,
  ...props
}: PropsType) => {
  const display = {
    carousel: <CampaignsCarousel data={data} />,
    grid: <CampaignsGrid data={data} />,
  };

  return (
    <Box
      as="section"
      w="full"
      bgColor="oilblue.10"
      position="relative"
      pt={24}
      pb={12}
      {...props}
    >
      <Container>
        {data[0].active_status && (
          <Box position="relative" rounded={24} overflow="hidden" mb={24}>
            <Image
              src={data[0].image.url}
              alt="Catálogo"
              layout="fill"
              objectFit="cover"
            />
          </Box>
        )}

        <Heading
          maxW={520}
          color="oilblue.300"
          px={6}
          mx="auto"
          textAlign="center"
          mb={data ? 8 : undefined}
        >
          {data
            ? 'Faça o download das últimas campanhas'
            : 'Em breve novas campanhas'}
        </Heading>
      </Container>
      {data && display[variant]}
      {withScroll && <Scroll file={ui.scroll.src.light.down} />}
    </Box>
  );
};
