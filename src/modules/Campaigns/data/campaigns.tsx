import client from '@/graphql/client';
import { GetCampaignsQuery } from '@/graphql/generated/graphql';
import { GET_CAMPAIGNS } from '@/graphql/queries';
import { sortByBoolean } from '@/utils/date';
import { CampaignModel } from '../entities/CampaignModel';

export const getCampaigns = async (
  first?: number,
): Promise<CampaignModel[]> => {
  const { campaigns } = await client.request<GetCampaignsQuery>(GET_CAMPAIGNS, {
    first: first || 9,
  });
  const sortedCampaigns = sortByBoolean(campaigns, 'active_status');
  return sortedCampaigns.map<CampaignModel>((c) => ({
    id: c.id,
    name: c.name,
    active_status: c.active_status,
    activityInterval: c.activityInterval,
    url: c.url,
    image: c.image,
  }));
};
