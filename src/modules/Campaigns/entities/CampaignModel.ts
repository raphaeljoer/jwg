type Image = {
  url: string;
};

export type CampaignModel = {
  id: string;
  name: string;
  active_status: boolean;
  activityInterval: string;
  url: string;
  image: Image;
};
