//chakra-ui
import {
  Badge,
  Box,
  Heading,
  useBreakpointValue,
  Image as CkImage,
  Flex,
} from '@chakra-ui/react';

//core components
import Text from '@/components/atoms/Text';
import Tooltip from '@/components/atoms/Tooltip';

//resources
import React from 'react';
import { FiDownload } from 'react-icons/fi';
import { getFormattedDate, isSameDate } from '@/utils/date';

//types
import ButtonCta from '../../../components/atoms/buttons/ButtonCta';
import Link from '../../../components/atoms/Link';
import { Catalog } from '../entities/catalog';

type CatalogCardType = {
  data: Catalog;
};

export const CatalogCard = ({ data }: CatalogCardType) => {
  const isLongName = useBreakpointValue({
    base: data.name.length > 12,
    lg: data.name.length > 20,
  });

  const isLaunch = (date: string) => {
    if (!date) return null;
    return isSameDate({
      dateToCompare: date,
      range: 'month',
    });
  };

  return (
    <Flex
      w="full"
      flexDir="column"
      minW={320}
      minH={345}
      maxW={{ base: 'xs', lg: 'sm' }}
      textDecoration="none"
      borderRadius="xl"
      overflow="hidden"
      css={{
        scrollSnapAlign: 'center',
        WebkitScrollSnapAlign: 'center',
      }}
    >
      {data.partner && (
        <Flex
          minH={180}
          borderTopRadius="lg"
          bgColor="white"
          align="center"
          justify="center"
        >
          <Box position="relative">
            <CkImage
              src={data.partner.logo}
              alt="Catálogo"
              layout="fill"
              width={240}
              height={88}
              objectFit="contain"
              objectPosition="center center"
            />
          </Box>
        </Flex>
      )}

      <Flex
        flexDir="column"
        h="full"
        bgColor="oilblue.500"
        borderBottomRadius="xl"
        p={6}
      >
        <Badge
          mt={-10}
          mb={6}
          p={2}
          w="fit-content"
          borderRadius="md"
          bgColor="orange.500"
          color="white"
          textTransform="uppercase"
        >
          {data.releaseDate
            ? isLaunch(data.releaseDate)
              ? 'LANÇAMENTO'
              : getFormattedDate({
                  date: data.releaseDate,
                  format: 'MMMM / YYYY',
                })
            : 'LANÇAMENTOS'}
        </Badge>

        <Tooltip label={data.name} placement="top-end" isDisabled={!isLongName}>
          <Heading
            as="h3"
            fontSize={28}
            color="oilblue.10"
            isTruncated
            mt="auto"
            mb={2}
          >
            {data.name}
          </Heading>
        </Tooltip>

        {data.description && (
          <Text fontSize={18} color="oilblue.50" mb={2}>
            {data.description}
          </Text>
        )}

        <Link href={data.url} target="_blank">
          <ButtonCta
            variant="hero"
            color="orange1"
            leftIcon={<FiDownload />}
            w="full"
            mt={4}
          >
            {'Baixar Catálogos'}
          </ButtonCta>
        </Link>
      </Flex>
    </Flex>
  );
};
