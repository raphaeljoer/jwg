//resources
import React from 'react';
//core-component
import { Catalog } from '../entities/catalog';
import { CatalogCard } from './CatalogCard';
import Carousel from '@/components/molecules/Carousel';

type CatalogsCarouselType = {
  data: Catalog[];
};

export const CatalogsCarousel = ({ data }: CatalogsCarouselType) => {
  return (
    <Carousel
      config={{
        scrollStep: 384,
        padding: { base: 4, md: 8, lg: 12 },
        spacing: { base: 4, md: 8 },
      }}
    >
      {data.map((d) => (
        <CatalogCard key={d.id} data={d} />
      ))}
    </Carousel>
  );
};
