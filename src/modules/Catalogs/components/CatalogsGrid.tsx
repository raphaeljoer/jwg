//resources
import React from 'react';
//core-component
import { Catalog } from '../entities/catalog';
import { CatalogCard } from './CatalogCard';
//utils
import Container from '@/components/molecules/Container';
import { Grid } from '@chakra-ui/react';

type CatalogsGridType = {
  data: Catalog[];
};

export const CatalogsGrid = ({ data }: CatalogsGridType) => {
  return (
    <Container>
      <Grid
        templateColumns={{
          base: 'repeat(1, 1fr)',
          md: 'repeat(2, 1fr)',
          xl: 'repeat(3, 1fr)',
        }}
        gap={8}
        rowGap={12}
        mt={24}
        justifyItems="center"
      >
        {data.map((d) => (
          <CatalogCard key={d.id} data={d} />
        ))}
      </Grid>
    </Container>
  );
};
