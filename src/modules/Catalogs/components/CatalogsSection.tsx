//chakra-ui
import { Box } from '@chakra-ui/react';
//core components
import Heading from '@/components/atoms/Heading';
//resources
import React from 'react';
//type
import { Catalog } from '../entities/catalog';
import { CatalogsCarousel } from './CatalogsCarousel';
import { CatalogsGrid } from './CatalogsGrid';
import Scroll from '@/components/atoms/Scroll';
import { ui } from '@/config/app';

const config = {
  title: 'Faça o download dos últimos lançamentos',
  noCards: 'Em breve novos catálogos',
  button: {
    label: 'Baixar catálogo',
  },
  seeMore: 'Veja todos os catálogos',
};

type CatalogVariant = 'carousel' | 'grid';

type CatalogsSectionType = {
  data: Catalog[];
  variant: CatalogVariant;
  withScroll?: boolean;
};

export const CatalogsSection = ({
  data,
  variant,
  withScroll,
}: CatalogsSectionType) => {
  const display = {
    carousel: <CatalogsCarousel data={data} />,
    grid: <CatalogsGrid data={data} />,
  };
  return (
    <Box
      as="section"
      w="full"
      bgColor="oilblue.10"
      position="relative"
      pt={24}
      pb={40}
    >
      <Heading
        mc
        tc
        maxW={768}
        color="oilblue.300"
        px={6}
        mb={data ? 8 : undefined}
      >
        {data ? config.title : config.noCards}
      </Heading>
      {data && display[variant]}
      {withScroll && <Scroll file={ui.scroll.src.light.down} />}
    </Box>
  );
};
