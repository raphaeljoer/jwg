import { Partner } from '@/@types/partner';

export type Catalog = {
  id: string;
  name: string;
  description?: string;
  category?: string;
  url: string;
  releaseDate?: string;
  partner: Partner;
};
