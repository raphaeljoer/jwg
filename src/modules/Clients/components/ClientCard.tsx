//chakra-ui
import { Badge, Box, Image as CkImage, Flex } from '@chakra-ui/react';

//core components
import Text from '@/components/atoms/Text';

//resources
import React from 'react';
//types
import { ClientModel } from '../entities/ClientModel';

type CatalogCardType = {
  data: ClientModel;
};

export const ClientCard = ({ data }: CatalogCardType) => {
  return (
    <Flex
      w="full"
      flexDir="column"
      minW={320}
      minH={345}
      maxW={{ base: 'xs', lg: 'sm' }}
      textDecoration="none"
      borderRadius="xl"
      overflow="hidden"
      css={{
        scrollSnapAlign: 'center',
        WebkitScrollSnapAlign: 'center',
      }}
    >
      {data.logo && (
        <Flex
          minH={180}
          borderTopRadius="lg"
          bgColor="white"
          align="center"
          justify="center"
        >
          <Box position="relative">
            <CkImage
              src={data.logo.url}
              alt="Catálogo"
              layout="fill"
              width={240}
              height={88}
              objectFit="contain"
              objectPosition="center center"
            />
          </Box>
        </Flex>
      )}

      <Flex
        flexDir="column"
        h="full"
        bgColor="oilblue.500"
        borderBottomRadius="xl"
        p={6}
      >
        <Badge
          mt={-10}
          mb={6}
          p={2}
          w="fit-content"
          borderRadius="md"
          bgColor="orange.500"
          color="white"
          textTransform="uppercase"
        >
          Área de Cobertura
        </Badge>

        {data.coverage && (
          <Text fontSize={18} fontWeight="bold" color="whiteAlpha.900" mb={2}>
            {data.coverage}
          </Text>
        )}
      </Flex>
    </Flex>
  );
};
