//resources
import React from 'react';
//core-component
import { ClientModel } from '../entities/ClientModel';
import { ClientCard } from './ClientCard';
import Carousel from '@/components/molecules/Carousel';

type ClientsCarouselType = {
  data: ClientModel[];
};

export const ClientsCarousel = ({ data }: ClientsCarouselType) => {
  return (
    <Carousel
      config={{
        scrollStep: 384,
        padding: { base: 4, md: 8, lg: 12 },
        spacing: { base: 4, md: 8 },
      }}
    >
      {data.map((d) => (
        <ClientCard key={d.id} data={d} />
      ))}
    </Carousel>
  );
};
