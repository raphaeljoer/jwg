//chakra-ui
import { Box, Image, Heading, SimpleGrid } from '@chakra-ui/react';
//resources
import React from 'react';
//type
import { ClientModel } from '../entities/ClientModel';
import { ClientsCarousel } from './ClientsCarousel';
import { ClientsGrid } from './ClientsGrid';
import Scroll from '@/components/atoms/Scroll';
import { ui } from '@/config/app';
import Container from '@/components/molecules/Container';

type ClientsVariant = 'carousel' | 'grid';

type ClientsSectionType = {
  data: ClientModel[];
  variant: ClientsVariant;
  withScroll?: boolean;
};

export const ClientsSection = ({
  data,
  variant,
  withScroll,
}: ClientsSectionType) => {
  const display = {
    carousel: <ClientsCarousel data={data} />,
    grid: <ClientsGrid data={data} />,
  };
  return (
    <Box
      as="section"
      w="full"
      bgColor="oilblue.10"
      position="relative"
      pt={24}
      pb={12}
    >
      <Heading textAlign="center">Parceiros</Heading>
      {data && display[variant]}
      <Container pt={8}>
        <SimpleGrid
          columns={{ base: 1, md: 2 }}
          justifyItems="center"
          spacing={10}
        >
          <Image
            src="/assets/ui/map-ce.svg"
            alt="Catálogo"
            width={380}
            height={495}
            objectFit="contain"
            objectPosition="center center"
          />

          <Heading
            color="oilblue.500"
            maxW={380}
            px={6}
            alignSelf="center"
            textAlign={{ base: 'center', md: 'left' }}
          >
            Cobertura por todo o nordeste
          </Heading>
        </SimpleGrid>
      </Container>
      {withScroll && <Scroll file={ui.scroll.src.light.down} />}
    </Box>
  );
};
