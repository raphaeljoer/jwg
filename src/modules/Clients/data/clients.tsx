import client from '@/graphql/client';
import { GetClientsQuery } from '@/graphql/generated/graphql';
import { GET_CLIENTS } from '@/graphql/queries';
import { ClientModel } from '../entities/ClientModel';

export const getClients = async (first?: number): Promise<ClientModel[]> => {
  const { clients } = await client.request<GetClientsQuery>(GET_CLIENTS, {
    first: first || 9,
  });
  return clients.map<ClientModel>((c) => ({
    id: c.id,
    name: c.name,
    logo: c.logo,
    coverage: c.coverage,
  }));
};
