type Logo = {
  url: string;
};

export type ClientModel = {
  id: string;
  name: string;
  logo: Logo;
  coverage: string;
};
