//chakra-ui
import { Box, Heading, useBreakpointValue } from '@chakra-ui/react';

//core components
import Text from '@/components/atoms/Text';
import Tooltip from '@/components/atoms/Tooltip';

//resources
import React from 'react';

//types
import { Testimonial } from '../entities/testimonial';
import { Video } from '@/components/atoms/Video';

type TestimonialCardType = {
  data: Testimonial;
};

export const TestimonialCard = ({ data }: TestimonialCardType) => {
  const { video_id, name, company } = data;

  const isLongName = useBreakpointValue({
    base: name.length > 15,
    lg: name.length > 22,
  });

  return (
    <Box w="full" textDecoration="none" borderRadius="xl" overflow="hidden">
      {video_id && <Video video_id={video_id} borderTopRadius="lg" />}

      <Box p={6} bgColor="oilblue.500" borderBottomRadius="xl">
        <Tooltip label={name} placement="top-end" isDisabled={!isLongName}>
          <Heading as="h3" fontSize={28} color="oilblue.10" isTruncated mb={2}>
            {name}
          </Heading>
        </Tooltip>

        <Text fontSize={18} color="orange.500">
          {company}
        </Text>
      </Box>
    </Box>
  );
};
