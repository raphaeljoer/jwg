import { Scroll } from '@/components/atoms/Scroll';
import { ui } from '@/config/app';
import { Box, Heading } from '@chakra-ui/react';
import React from 'react';
import { Testimonial } from '../entities/testimonial';
import { TestimonialsGrid } from './TestimonialsGrid';

type TestimonialSectionType = {
  data: Testimonial[];
  withScroll?: boolean;
};

const title = {
  value: 'Depoimentos',
  noValue: 'Em breve depoimentos de nossos clientes',
};

const displayTitle = (data: Testimonial[]) => (
  <Heading
    color="oilblue.500"
    textAlign="center"
    px={8}
    maxW={768}
    m="auto"
    mb={data ? 8 : undefined}
  >
    {data ? title.value : title.noValue}
  </Heading>
);

export const TestimonialSection = ({
  withScroll,
  data,
}: TestimonialSectionType) => {
  return (
    <Box
      as="section"
      minW="100vw"
      bgColor="ice.50"
      position="relative"
      pt={24}
      pb={40}
    >
      {displayTitle(data)}
      {data && <TestimonialsGrid data={data} />}
      {withScroll && <Scroll file={ui.scroll.src.light.down} />}
    </Box>
  );
};
