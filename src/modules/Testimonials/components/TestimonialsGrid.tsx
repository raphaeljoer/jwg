import Container from '@/components/molecules/Container';
import { Grid } from '@chakra-ui/react';
import React from 'react';
import { Testimonial } from '../entities/testimonial';
import { TestimonialCard } from './TestimonialCard';

type TestimonialsGridType = {
  data: Testimonial[];
};

export const TestimonialsGrid = ({ data }: TestimonialsGridType) => {
  return (
    <Container>
      <Grid
        templateColumns={{
          base: 'repeat(1, 1fr)',
          md: 'repeat(2, 1fr)',
          lg: 'repeat(3, 1fr)',
        }}
        gap={8}
        rowGap={12}
        mt={24}
        justifyItems="center"
      >
        {data.map((d) => (
          <TestimonialCard key={d.video_id} data={d} />
        ))}
      </Grid>
    </Container>
  );
};
