import { Testimonial } from '../entities/testimonial';

export const testimonials: Testimonial[] = [
  {
    video_id: 'Z3XDntRd_Uc',
    name: 'Claudia Bezerra',
    company: 'Diretora Financeira - Bezerra Oliveira',
  },
  {
    video_id: 'Ge79hZOEKDA',
    name: 'Leandro Machado',
    company: 'Diretor Comercial - Padre Cícero',
  },
  {
    video_id: '1KB2aKajUWc',
    name: 'Andrea',
    company: 'Gerente Comercial - Eletroparts',
  },
];
