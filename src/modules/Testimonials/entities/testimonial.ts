export type Testimonial = {
  video_id: string;
  name: string;
  company: string;
};
