//core components
import Cta from '@/components/molecules/Cta';
import Hero from '@/components/molecules/Hero';
import Layout from '@/components/Layout';
import NavBar from '@/components/molecules/NavBar';
import Partners from '@/components/molecules/Partners';
import Footer from '@/components/molecules/Footer';
//resources
import React from 'react';
import SEO from '@/config/seo';
import { NextSeo } from 'next-seo';
import { next } from '@/config/app';
import { GetStaticProps } from 'next';
//data
import { getCampaigns } from '@/modules/Campaigns/data/campaigns';
//types
import { TestimonialSection } from '@/modules/Testimonials/components/TestimonialSection';
import { testimonials } from '@/modules/Testimonials/data/testimonials';
import { CampaignModel } from '@/modules/Campaigns/entities';
import { CampaignsSection } from '@/modules/Campaigns/components';

const heroProps = {
  element: '/assets/ui/element-02.svg',
};

const ctaProps = {
  title: 'Conheça nossas campanhas',
};

interface Props {
  campaigns: CampaignModel[];
}

export default function CampanhasPage({ campaigns }: Props) {
  return (
    <Layout>
      <NextSeo {...SEO.page.campanhas} />
      <NavBar />
      <Hero {...heroProps}>
        <Cta {...ctaProps} />
      </Hero>
      <CampaignsSection data={campaigns} variant="grid" py={32} />
      <TestimonialSection data={testimonials} withScroll />
      <Partners />
      <Footer />
    </Layout>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: {
      campaigns: await getCampaigns(),
    },
    revalidate: next.revalidate.oneMinute,
  };
};
