//core components
import Cta from '@/components/molecules/Cta';
import Hero from '@/components/molecules/Hero';
import Layout from '@/components/Layout';
import NavBar from '@/components/molecules/NavBar';
import Partners from '@/components/molecules/Partners';
import Footer from '@/components/molecules/Footer';
//resources
import React from 'react';
import SEO from '@/config/seo';
import { NextSeo } from 'next-seo';
import { next } from '@/config/app';
import { GetStaticProps } from 'next';
//data
import { getCatalogs } from '@/modules/Catalogs/data/catalogs';
//types
import { CatalogsSection } from '@/modules/Catalogs/components/CatalogsSection';
import { Catalog } from '@/modules/Catalogs/entities/catalog';
import { TestimonialSection } from '@/modules/Testimonials/components/TestimonialSection';
import { testimonials } from '@/modules/Testimonials/data/testimonials';

const heroProps = {
  element: '/assets/ui/element-02.svg',
};

const ctaProps = {
  title: 'Faça o download dos últimos catálogos lançados',
  description:
    'Entregamos confiança, credibilidade e inovação por todo nordeste',
};

interface PecasPageType {
  catalogs: Catalog[];
}

export default function CatalogosPage({ catalogs }: PecasPageType) {
  return (
    <Layout>
      <NextSeo {...SEO.page.catalogos} />
      <NavBar />
      <Hero {...heroProps}>
        <Cta {...ctaProps} />
      </Hero>
      <CatalogsSection data={catalogs} variant="grid" withScroll />
      <TestimonialSection data={testimonials} withScroll />
      <Partners />
      <Footer />
    </Layout>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: {
      catalogs: await getCatalogs(),
    },
    revalidate: next.revalidate.oneMinute,
  };
};
