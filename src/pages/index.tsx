//chakra-ui
import { Button, Icon } from '@chakra-ui/react';
//core components
import Cta from '@/components/molecules/Cta';
import Hero from '@/components/molecules/Hero';
import { CatalogsSection } from '@/modules/Catalogs/components/CatalogsSection';
import DataImpact from '@/components/molecules/DataImpact';
import Footer from '@/components/molecules/Footer';
import { TestimonialSection } from '@/modules/Testimonials/components/TestimonialSection';
//resources
import React from 'react';
import SEO from '@/config/seo';
import { NextSeo } from 'next-seo';
import { GetStaticProps } from 'next';
import { next, whatsapp } from '@/config/app';
//data
import { getCatalogs } from '@/modules/Catalogs/data/catalogs';
import { getClients } from '@/modules/Clients/data/clients';
//types
import ICtaProps from '@/@types/cta';
import Layout from '@/components/Layout';
import { testimonials } from '@/modules/Testimonials/data/testimonials';
import { Catalog } from '@/modules/Catalogs/entities/catalog';
import { FaWhatsapp } from 'react-icons/fa';
import { ClientModel } from '@/modules/Clients/entities';
import { ClientsSection } from '@/modules/Clients/components/ClientsSection';

const heroProps = {
  image: '/assets/img/picture/banner-pai-e-filho.png',
};

const ctaProps: ICtaProps = {
  color: 'whiteAlpha.800',
  upTitle: 'Desde 2000',
  title: 'Tradição e inovação é a nossa entrega',
  description: 'Somos o elo entre fornecedores e redes de distribuição.',
};

interface HomeProps {
  catalogs: Catalog[];
  clients: ClientModel[];
}

export default function Home({ catalogs, clients }: HomeProps) {
  return (
    <Layout>
      <NextSeo {...SEO.page.home} />
      <Hero {...heroProps}>
        <Cta {...ctaProps}>
          <Button
            variant="primary"
            size="lg"
            maxW="xs"
            mx={{ base: 'auto', lg: 0 }}
            mt={10}
            leftIcon={<Icon as={FaWhatsapp} mr={2} fontSize={32} />}
            onClick={() => (location.href = whatsapp.link)}
          >
            Fale com o especialista agora
          </Button>
        </Cta>
      </Hero>
      <CatalogsSection data={catalogs} variant="carousel" />
      <DataImpact />
      <ClientsSection data={clients} variant="carousel" withScroll />
      <TestimonialSection data={testimonials} />
      <Footer />
    </Layout>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: {
      catalogs: await getCatalogs(),
      clients: await getClients(),
    },
    revalidate: next.revalidate.oneMinute,
  };
};
