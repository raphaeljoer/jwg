import { ButtonProps, ComponentStyleConfig } from '@chakra-ui/react';

export const baseStyle: ButtonProps = {
  rounded: 'xl',
  cursor: 'pointer',
};

const primary: ButtonProps = {
  color: 'oilblue.800',
  bgColor: 'orange.500',
  border: '1px solid',
  borderColor: 'orange.500',
  _hover: {
    color: 'orange.500',
    bgColor: 'transparent',
  },
};

const secondary: ButtonProps = {
  color: 'orange.500',
  bgColor: 'transparent',
  border: '1px solid',
  borderColor: 'orange.500',
  _hover: {
    color: 'oilblue.800',
    bgColor: 'orange.500',
  },
};

const sizes = {
  lg: {
    fontSize: 18,
    h: 14,
  },
};

export const Button: ComponentStyleConfig = {
  baseStyle,
  defaultProps: {},
  sizes,
  variants: {
    primary,
    secondary,
  },
};

export default Button;
