import { ChakraTheme, extendTheme } from '@chakra-ui/react';
//config
import { config } from './config';
//foundations
import { fonts } from './foundations/fonts';
import { colors } from './foundations/colors';
//components
import { Button } from './components/button';

export const theme: ChakraTheme = extendTheme({
  styles: {
    global: {
      html: {
        scrollBehavior: 'smooth',
        fontSize: 18,
      },
    },
  },
  fonts,
  config,
  colors,
  components: {
    Button,
  },
});

export default extendTheme(theme);
